import React from 'react';
import ReactDOM from 'react-dom';

import {Provider} from 'react-redux';
import {Router, Route} from 'react-router';
import {createStore, applyMiddleware} from 'redux';
import reducers from 'reducers'
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import {browserHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';

import Layout from 'containers/layout';
import layoutPhone from 'containers/layoutphone';
import layoutBasket from 'containers/layoutbasket';
import Phones from 'containers/phones';
import Phone from 'containers/phone';
import Cart from 'containers/cart';

const store = createStore(reducers, composeWithDevTools(
    applyMiddleware(thunk)
));

const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Route component={Layout}>
                <Route component={Phones} path='/'/>
                <Route component={Phones} path='/categories/:id'/>
            </Route>

            <Route component={layoutPhone}>
                <Route path='/phones/:id' component={Phone} />
            </Route>

            <Route component={layoutBasket}>
                <Route path='/basket' component={Cart} />
            </Route>
        </Router>
    </Provider>
    ,document.getElementById('app'));