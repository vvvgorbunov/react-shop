'use strict';

const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const root = __dirname + "/../";

const path = require('path');
const webpack = require('webpack');

module.exports = {

    entry: [
        root + 'src/app/index.js'
    ],

    output: {
        path: 'dist',
        publicPath: '/',
        filename: 'bundle.js'
    },
    
    debug: true,

    devtool: false,

    module: {
        loaders: [
            {
                test: [/\.jsx$/, /\.js$/],
                include: [
                    path.resolve(root, "src", "app"),
                    path.resolve(root, "node_modules/flash-notification-react-redux")
                ],
                loader: 'babel-loader'
            },
            {
                test: [/\.scss$/, /\.css$/],
                loader: 'css?localIdentName=[path]!postcss-loader!sass',
            }
        ]
    },

    plugins: [

        new webpack.optimize.UglifyJsPlugin({
            compress: {warnings: false},
            comments: false,
        }),

        new webpack.DefinePlugin({
            'process.env': {
                'ENV': JSON.stringify('prod'),
                'NODE_ENV': JSON.stringify('production')
            }
        }),

        new CleanWebpackPlugin(path.resolve(root, 'dist'), {
            root: root,
            verbose: true
        }),

        new CopyWebpackPlugin([
            {
                from: root + 'src/static',
                to: root + 'dist/'
            },
            {
                from: root + 'src/html/dist/static',
                to: root + 'dist/html/'
            },
            {
                from: root + 'src/html/dist/content',
                to: root + 'dist/html/dist/content'
            },
            {
                from: root + 'src/locales',
                to: root + 'locales/'
            }
        ], {}),

        new HtmlWebpackPlugin({
            template: root + 'src/index.html',

            title: 'TDCS PROFESSIONALS’ CENTER',
            hash: new Date().getTime(),
            static_html_path: '/html/',
            static_path: '/'
        })
    ]
};