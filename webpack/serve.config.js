'use strict';

const CopyWebpackPlugin = require('copy-webpack-plugin');
const HotModuleReplacementPlugin = require('webpack/lib/HotModuleReplacementPlugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const polyfill = require("babel-polyfill");
const root = __dirname + "/../";

var path = require('path');
var webpack = require('webpack');

module.exports = {

    entry: [
        //'webpack-dev-server/client?http://localhost:8080/',
        'webpack/hot/only-dev-server',
        'babel-polyfill',
        root + 'src/index.js'
    ],

    output: {
        filename: 'bundle.js',
        publicPath: '/'
    },

    debug: true,

    devtool: 'source-map',

    devServer: {
        inline: true,
        hot: true,
        port: 9090,
        historyApiFallback: true,
        contentBase: path.resolve(root, "src")
    },

    module: {
        loaders: [
            {
                test: [/\.jsx$/, /\.js$/],
                include: [
                    path.resolve(root, "src"),
                ],
                loader: 'babel-loader'
            },
            {
                test: [/\.scss$/, /\.css$/],
                loader: 'css?localIdentName=[path]!postcss-loader!sass',
            },
        ]
    },

    plugins: [

        new webpack.DefinePlugin({
            'process.env': {
                'ENV': JSON.stringify('local')
            }
        }),

        new HtmlWebpackPlugin({
            template: root + 'src/index.html',
            hash: new Date().getTime(),
            static_html_path: './',
            static_path: './'
        }),

        //new HotModuleReplacementPlugin(),
    ]
};