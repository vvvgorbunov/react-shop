'use strict';

(function() {
    var childProcess = require("child_process");
    var oldSpawn = childProcess.spawn;
    function mySpawn() {
        console.log('spawn called');
        console.log(arguments);
        var result = oldSpawn.apply(this, arguments);
        return result;
    }
    childProcess.spawn = mySpawn;
})();

const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const root = __dirname + "/../";

const path = require('path');
const webpack = require('webpack');

module.exports = {

    entry: [
        root + 'src/index.js'
    ],

    output: {
        path: 'dist',
        publicPath: '/',
        filename: 'bundle.js'
    },

    debug: true,

    devtool: 'source-map',

    module: {
        loaders: [
            {
                test: [/\.jsx$/, /\.js$/],
                include: [
                    path.resolve(root, "src"),
                    path.resolve(root, "node_modules/flash-notification-react-redux")
                ],
                loader: 'babel-loader'
            },
            {
                test: [/\.scss$/, /\.css$/],
                loader: 'css?localIdentName=[path]!postcss-loader!sass',
            }
        ]
    },

    plugins: [

        new webpack.DefinePlugin({
            'process.env': {
                'ENV': JSON.stringify('dev'),
                'NODE_ENV': JSON.stringify('development')
            }
        }),

        new CopyWebpackPlugin([
            {
                from: root + 'html/dist/',
                to: root + 'dist/html'
            },
        ], {}),

        new CopyWebpackPlugin([
            {
                from: root + 'src/css/',
                to: root + 'dist/css'
            },
        ], {}),

        new HtmlWebpackPlugin({
            template: root + 'src/index.html',

            title: 'DEV: CyberClinic',
            hash: new Date().getTime(),
            static_html_path: '/html/static/',
            static_path: '/'
        })
    ]
};